const express = require("express");
const app = express();
const port = 3000;
const { transporter } = require("./mailConfig");
const bodyParser = require("body-parser");

const { Expo } = require("expo-server-sdk");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.post("/mail/send", async ({ body }, res) => {
	const { to, subject, text } = body;
	if (!to || !subject || !text) {
		return res.status(400).send('"to", "subject" or "text" are missing');
	}
	try {
		await transporter.sendMail({
			to,
			subject,
			text,
		});
		console.log({ to });
		console.log({ subject });
		console.log({ text });
		return res.sendStatus(200);
	} catch (err) {
		console.log(err);
		return res.status(500).send("Something went wrong");
	}
});

app.post("/push/send", async ({ body }, res) => {
	const { pushToken, title, text, channelId } = body;

	if (!pushToken || !title || !text || !channelId) {
		return res
			.status(400)
			.send('"pushToken", "title", "text" or "channelId" are missing');
	}

	const message = {
		to: pushToken,
		sound: "default",
		title,
		body: text,
		channelId,
	};

	try {
		const expo = new Expo();
		await expo.sendPushNotificationsAsync([message]);
		res.sendStatus(200);
	} catch (error) {
		console.log(error);
		return res.status(500).send("Something went wrong");
	}
});

app.listen(port, () =>
	console.log(`Notification service is listening at ${port}`)
);
